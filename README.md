# I3 Solar
**Mission Statement:**
<br>"We are looking to enhance the information for potential solar power users to increase renewable energy, by modelling solar potential."

---

**Abstract:**

<br>**Modelling the solar potential in an urban environment at the example of Salzburg**
<br>The need of renewable energy sources is heavily increasing, caused by the climate change. Photovoltaic panels posed to be a good way to produce both electric and thermal energy by making use of existing rooftops, thus avoiding additional land use. There are already multiple different modelling approaches for the solar potential of urban areas, but larger scale models do not take weather conditions or shadowing (by landscape and cityscape) into account. Therefore, this study aims to model the solar potential in the city of Salzburg considering the mentioned parameters and validating the modelled solar potential with measured data to estimate the reliability of the results. For this purpose, a GIS-based approach utilising a 3D-model of the city, weather and irradiance data is used.
<br>The results are expected to improve the information about potential photovoltaic locations. To easily address both houseowners, who are possibly interested in a photovoltaic system, as well as local decision-makers, a web-map publication is most suitable.
<br> Further information [here.](https://git.sbg.ac.at/s1070944/i3-solar/wikis/WP-2-Set-the-goal-&-build-foundation/T-2.2-Writing-the-Project-abstract)